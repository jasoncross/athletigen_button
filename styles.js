/*
	~ Global Variables used ONLY for Styles
*/

'use strict'

//Size Variants
export const fontSizeSmallest = 14;

//Font Variants
export const robotoRegular = 'Roboto-Regular';

//Colour Variants
export const baseBlueColor = '#468ad2';
export const baseFlatBlueColor = '#7196F8';
export const baseDarkGreyColor = '#646464';
export const baseGreyColor = 'rgb(220,220,220)';
export const baseBlackColor = 'rgb(20,20,20)';
export const baseWhiteColor = '#ffffff';
export const baseParagraphColor = baseBlackColor;
export const baseErrorAccentColor = baseWhiteColor;

//Spinner Variants
export const spinnerDefaultButtonSize = 20;
export const spinnerDefaultType = 'Arc';