/*
	~ React Component for Flat Button
*/

'use strict'

//Importation of Application Requirements
import React, { Component } from 'react';

import { 
	Text,
	View,
	StyleSheet,
	TouchableHighlight,

} from 'react-native';

//Import Styles
import 
{ 
	baseBlueColor, 
	baseGreyColor,
	baseBlackColor,
	fontSizeSmallest,
	baseDarkGreyColor,
	baseFlatBlueColor,
	spinnerDefaultType,
	baseErrorAccentColor,
	spinnerDefaultButtonSize,

} from './styles';

import Spinner from 'react-native-spinkit';


export default class Button extends Component {


	//rN Lifecycle ---------------------------

	constructor(props) {
		super(props);

		this.state = {
			buttonIsPressed: false,
			isBusy: false,
		};
	}


	render() {
		return(
			<TouchableHighlight
				style={[
					ownedStyles.buttonWrapper, 
					this.props.wrapperStyle,
					this.props.disabled ? {backgroundColor: baseGreyColor} : {backgroundColor: (this.props.solidBackground || baseFlatBlueColor)},
					this.props.disabled ? {borderColor: baseGreyColor} : {borderColor: this.props.borderColor || baseGreyColor},
				]}
				onPress={this.props.disabled ? null : this.props.callback}
				underlayColor={this.props.pressBackColor || baseFlatBlueColor}
				onShowUnderlay={this.toggleButtonPressedState.bind(this)}
				onHideUnderlay={this.toggleButtonPressedState.bind(this)}>

				<View style={{flexDirection: 'column'}}>
					<Text style={[
									ownedStyles.buttonText, 
									this.buttonPress(),
									this.props.style,
									{justifyContent: 'center'},
									this.toggleBusyStyles()
								]}>

						{this.state.isBusy ? null : this.props.children}
					</Text>

					{this.toggleSpinner()}
				</View>

			</TouchableHighlight>
		);
	}



	//Functionality --------------------------

	setToBusy() {
		if(typeof this._reactInternalInstance !== 'undefined') {
			this.setState({
				isBusy: true,
			});
		}
	}


	setToIdle() {
		if(typeof this._reactInternalInstance !== 'undefined') {
			this.setState({
				isBusy: false,
			});
		}
	}


	toggleBusyStyles() {
		if(this.state.isBusy) {
			return {height: 0, paddingTop: 0};
		
		} else { return {}; }
	}


	toggleSpinner() {
		if(this.state.isBusy) {
			return (
				<Spinner 
	                isVisible={true} 
	                size={spinnerDefaultButtonSize}
	                type={spinnerDefaultType} 
	                style={ownedStyles.splashSpinningGif}
	                color={this.props.spinnerBackground ? this.props.spinnerBackground : baseBlackColor} />
			);
		
		} else {
			return null;
		}
	}


	toggleButtonPressedState() {
		this.setState({buttonIsPressed: !this.state.buttonIsPressed});
	}


	buttonPress() {
		if(this.state.buttonIsPressed) {
			return {color: this.props.pressTextColor} || {color: baseBlackColor};
		}
	}

}


//Default Styles
const ownedStyles = StyleSheet.create({
	buttonWrapper: {
		borderColor: baseBlueColor,
		borderWidth: 2,
		borderStyle: 'solid',
		borderRadius: 5,
	},
	baseHeader: {
		marginTop: 40,
		marginBottom: 20,
		letterSpacing: 1,
		textAlign: 'center',
	},
	buttonText: {
		alignSelf: 'center',
		justifyContent: 'center',

		paddingTop: 10,
		paddingBottom: 10,

		color: baseDarkGreyColor,
		fontSize: fontSizeSmallest,
		fontStyle: 'normal',
		fontWeight: 'bold',
		textAlign: 'center',
	},
	baseErrorColor: {
		color: baseErrorAccentColor,
	},
	blackText: { 
		color: baseBlackColor 
	},
	splashSpinningGif: {
		alignSelf : 'center',
		paddingBottom : 35,
		justifyContent: 'center',
	},
});


