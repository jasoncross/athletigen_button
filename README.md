# **Athletigen Default Mobile Button**
#### Using React Native
_```By: Athletigen Engineering Dept.```_

>This button is intended to be used as the default button thoughout all current mobile applications on iOS and Android.

# Use in Project

Temporary use
```bash
npm install @athletiscope/button
```
Permenant Use
```bash
npm install --save @athletiscope/button
```
_If you wish to clone this project directly_: ```https://bitbucket.org/christian_bowyer/athletigen_button```

## `API` Reference
Flat, filled or empty, press responsive button. Default styles include rounded borders and default colors.

Props
-----

>All prop values must be standard camel case

###### ```wrapperStyle```
Will give a style to the surrounding view wrapper of component

Type: `style obj: {}` &nbsp; --- &nbsp;
Example: `wrapperStyle={{height: 20, width: 30}}`
---

###### ```style```
Will give a style to the inner text element of component

Type: `style obj: {}` &nbsp; --- &nbsp;
Example: `style={{color: '#000000', width: 30}}`
---

###### ```solidBackground```
Will give a solid background colour to the inside of the button UI, before any presses are made

```default-- color: 'transparent'```

Type: `color string: ''` &nbsp; --- &nbsp;
Example: `solidBackground={'#000000'}`
---

###### ```borderColor```
Will give a border colour to the outside of the button UI

```default-- borderStyle: 'solid'```

Type: `color string: ''` &nbsp; --- &nbsp;
Example: `borderColor={'#000000'}`
---

###### ```pressBackColor```
Will use the given colour to replace background color inside of the button UI, after all presses are made

```default-- backgroundColor: '#000000'```

Type: `color string: ''` &nbsp; --- &nbsp;
Example: `pressBackColor={'#000000'}`
---

###### ```pressTextColor```
Will use the given colour to replace text color inside of the button UI, after all presses are made

```default-- color: '#ffffff'```

Type: `color string: ''` &nbsp; --- &nbsp;
Example: `pressTextColor={'#ffffff'}`
---

###### ```spinnerBackground```
Will use the given colour to replace loading spinner color inside of the button UI, during busy state

```default-- color: '#000000'```

Type: `color string: ''` &nbsp; --- &nbsp;
Example: `spinnerBackground={'#000000'}`
---

###### ```disabled```
Will turn off button functionality and provide a default greyed out interior style

Type: `boolean` &nbsp; --- &nbsp;
Example: `disabled={true}`
---

###### ```callback```
Will trigger given callback function, after a press has been made

Type: `callback function() {}` &nbsp; --- &nbsp;
Example: `callback={() => { console.log('I was pressed!'); }}`
---